<?php

 $config = [
 	"database" => [
	 		"host" => "localhost",
	 		"username" => "root",
	 		"password" => "root",
	 		"database" => "blog"
	 ],
	"site_name" => "LoginSystem",
	"url" => "http://stage.alex/php/Login" // Url without /
 ];
 
/**
* Try to create a new PDO connection
*/
try {
	$db = new PDO('mysql:dbname='.$config['database']['database'].';host=' . $config['database']['host'], $config['database']['username'], $config['database']['password']);
}
catch(PDOexception $e) {
	echo 'Couldn\'t connect to the mysql server with details: <br />' . $e->getMessage(), 'Database connection error';
}