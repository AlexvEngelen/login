<?php


class User {

	private $fields = [ ];


	public function __construct( $id ) {
		global $db;
		$query = $db->prepare( 'SELECT * FROM users WHERE id=:id' );
		$query->execute( [ ':id' => $id ] );
		if ( $query->rowCount() == 1 ) {
			foreach ( $query->fetch() as $key => $value ) {
				$this->fields[ $key ] = $value;
			}
		}
	}


	public function data( $key ) {
		if ( isset( $this->fields[ $key ] ) && $key !== 'password' ) {
			return $this->fields[ $key ];
		}

		return false;
	}


	public static function byUsername( $username ) {
		global $db;
		$query = $db->prepare( 'SELECT * FROM users WHERE username = :username' );
		$query->execute( [ ':username' => $username ] );
		if ( $query->rowCount() == 1 ) {
			$getUserId = $query->fetch();

			return new User( $getUserId[ 'id' ] );
		}

		return false;
	}


	public function login( $password ) {
		return password_verify( $password, $this->fields[ 'password' ] );
	}


	public static function loggedIn() {
		return isset( $_SESSION[ 'id' ] );
	}

	public function logout() {
		global $config;
		if ( isset( $_SESSION[ 'id' ] ) ) {
			unset( $_SESSION[ 'id' ] );
		}
		header( "Location: {$config['url']}" );
	}

	public static function register( $username, $password ) {
		global $db, $config;
		$query = $db->prepare( 'SELECT `username` FROM `users` WHERE `username` = :username' );
		$query->execute( [ ':username' => $username ] );
		if ( $query->rowCount() == 0 ) {
			if ( strlen( $password ) > 5 ) {
				$insertQuery = $db->prepare( '
				INSERT INTO
					`users`
				(
					username,
					password,
					ip_reg,
					ip_last
				)
				VALUES
				(
					:username,
					:password,
					:ip_reg,
					:ip_last
				)
				' );
				$insertQuery->execute( [
					':username' => $username,
					':password' => password_hash( $password, PASSWORD_BCRYPT ),
					':ip_reg'   => getIP(),
					':ip_last'  => getIP()
				] );

				return [ 'msg' => 'U bent succesvol ingelogd.', 'id' => $db->lastInsertId() ];
			}

			return [ 'msg' => 'Uw wachtwoord moet uit minimaal 6 karakters bestaan.' ];
		}

		return [ 'msg' => 'Deze gebruikersnaam is al in gebruik.' ];
	}
}
