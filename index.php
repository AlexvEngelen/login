<?php

if ( session_status() == PHP_SESSION_NONE ) {
	session_start();
}

require_once 'config.php';
require_once 'functions.php';
require_once 'classes/User.php';

if ( User::loggedIn() ) {
	$user = new User( $_SESSION[ 'id' ] );
}

$error = null;
include "content/header.php";
if ( isset( $_GET[ 'p' ] ) ) {
	$page         = $_GET[ 'p' ];
	$allowedFiles = [ '404', 'home', 'index', 'logout', 'register' ];
	if ( $page && in_array( $page, $allowedFiles ) ) {
		include( "pages/$page.php" );
	} else {
		include( 'pages/index.php' );
	}
} else {
	include( 'pages/index.php' );
}
include 'content/footer.php';
